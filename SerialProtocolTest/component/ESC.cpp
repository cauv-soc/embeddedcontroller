#include "mbed.h"
#include "ESC.h"
#include "platform/mbed_thread.h"
#include "platform/mbed_debug.h"

ESC::ESC(PinName pin, bool init)
{
    PWMpin = new PwmOut(pin);
    PWMpin->period_ms(PWM_PERIOD);
    PWMpin->write(0.0);
    speed = 0;
    if (init)
        initiationSequence();    
}
    
void ESC::setSpeed(int percent)
{
    if (!engaged)
    {
        debug("ERROR: attempting to use a disengaged motor");
        return;
    }
    percent = clamp(percent);
    speed = percent;
    int pulseWidth = mapToPulse(percent);
    float dutyCycle = (float)pulseWidth/1000.0/(float)PWM_PERIOD;
    debug("duty cycle: %f\r\n", dutyCycle);
    PWMpin->write(dutyCycle);
}

// as of now, it is assumed that the motor is uni-directional
void ESC::incrementSpeed()
{
    setSpeed(speed + SPEED_STEP);
}

void ESC::decrementSpeed()
{
    setSpeed(speed - SPEED_STEP);
}

int ESC::getSpeed()
{
    return speed;
}

void ESC::initiationSequence()
{
    engaged = true;
    speed = 0;
    PWMpin->write(0);
    debug("Arming sequence start\r\n");
    thread_sleep_for(POWER_ON_WAIT);
    
    debug("Start Ramping Up\r\n");
    debug("This will take %d ms\r\n", 30 * ARM_STEP_DELAY / SPEED_STEP);
    while (getSpeed() < 30)
    {
        incrementSpeed();
        thread_sleep_for(ARM_STEP_DELAY);
    }
    
    debug("Start Ramping Down\r\n");
    while (getSpeed() > 0)
    {
        decrementSpeed();
        thread_sleep_for(ARM_STEP_DELAY);
    }
    
    debug("End arming squence\r\n");
    thread_sleep_for(END_ARM_WAIT);
    
    debug("Motor ready!\r\n");
 
}

void ESC::calibrationSequence()
{
    if (engaged)
    {
        debug("ERROR: attempt to calibrate an engaged motor");
        return;
    }
    debug("calibrationSequence Start");
    // content to be filled in.
}

void ESC::disengage()
{
    speed = 0;
    PWMpin->write(0.0);
    debug("Motor disengaged\r\n");
    engaged = false;
}

void ESC::engage()
{
    initiationSequence();
    engaged = true;
}

bool ESC::is_engaged()
{
    return engaged;
}

int ESC::mapToPulse(int percent)
{
    percent = clamp(percent);
    int pulseWidth = (MAX_PULSE_WIDTH - MIN_PULSE_WIDTH)*percent/100 + MIN_PULSE_WIDTH;
    return pulseWidth;
}

int ESC::clamp(int percent)
{
    if (percent > 100)
        return 100;
    if (percent < 0)
        return 0;
    return percent;
}