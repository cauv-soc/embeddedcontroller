import cv2
import numpy as np
cap = cv2.VideoCapture(0)
ret, cvImg = cap.read()
cap.release()
img_encode = cv2.imencode('.jpg', cvImg)

data_encode = np.array(img_encode[1])
str_encode = data_encode.tostring()

nparr = np.frombuffer(str_encode, np.uint8)
img_decode = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
cv2.imshow("img_decode", img_decode)
cv2.waitKey()



