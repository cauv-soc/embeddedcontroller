// This is a PWM based implementation.
// However, considering the extremely low frequency required, a non-PWM based 
// implementation is definitely feasible

#ifndef ESC_H
#define ESC_H

#include "mbed.h"

//need to define some timing parameters
#define MIN_PULSE_WIDTH 1000//us
#define MAX_PULSE_WIDTH 2000//us
#define PWM_PERIOD 20//ms

#define POWER_ON_WAIT 2000 //ms
#define ARM_STEP_DELAY 500//ms
#define END_ARM_WAIT 1000//ms
#define DISENGAGE_WAIT 2000//ms

#define SPEED_STEP 2// percent, used in increment() and decrement()

class ESC
{
public:

    ESC(PinName pin, bool init = true);
    
    void setSpeed(int percent);
    
    // as of now, it is assumed that the motor is uni-directional
    void incrementSpeed();
    
    void decrementSpeed();

    int getSpeed();
    
    void calibrationSequence();
    
    void disengage();
    
    void engage();
    
    bool is_engaged();
    
private:
    void initiationSequence();
    
    // clamping will be managed here
    int mapToPulse(int percent);
    
    int clamp(int percent);
    
    bool engaged;
    
    int speed;
    
    PwmOut* PWMpin;
};


#endif