import PythonComponent.gRPC.communication_pb2 as communication_pb2
import PythonComponent.gRPC.communication_pb2_grpc as communication_pb2_grpc
import logging
from PythonComponent.ImageProc.videoCaptureAsync import VideoCaptureAsync
import cv2
import numpy as np
from PythonComponent.utilities.queueUtilities import *
from PythonComponent.gRPC.containers import *
import pickle


class CommunicationServicer(communication_pb2_grpc.CommunicationServicer):
    def __init__(self):
        # assume that things will be set properly after the constructor
        self.imageStreamingQ = None
        self.groundStationInstructionQ = None
        self.statusUpdateQ = None

        # self.vidCapture = _vidCapture
        pass  # looks like that I do not have to do anything at this moment

    def setImageStreamingQ(self, imageStreamingQ):
        self.imageStreamingQ = imageStreamingQ

    def setGroundStationInstructionQ(self, groundStationInstructionQ):
        self.groundStationInstructionQ = groundStationInstructionQ

    def setStatusUpdateQ(self, statusUpdateQ):
        self.statusUpdateQ = statusUpdateQ

    def Echo(self, request, context):
        logging.info('Communication server received: ' + request.Msg)
        return request

    def StatusReport(self, request, context):
        logging.info('Status Requested')
        # retrieve the image
        cvImg = readLatest(self.imageStreamingQ)
        str_encode = bytes()
        if not (cvImg is None):
            logging.info('Img Obtained')
            img_encode = cv2.imencode('.jpg', cvImg)
            data_encode = np.array(img_encode[1])
            str_encode = data_encode.tostring()
            # logging.info('Img encoded to string')
        # retrieve the status report from main logic thread
            # nothing as of now
        returnMsg = communication_pb2.Status(encodedImg=str_encode)
        logging.info('Return message packaged')
        return returnMsg

    def statusReport(self, request, context):
        logging.info('Status Requested')
        # retrieve the image
        cvImg = readLatest(self.imageStreamingQ)
        # skip encoding for now
        statusReport = StatusReportContainer()
        # do not really have to care about whether it is None here
        statusReport.imgFrameRaw = cvImg
        binaryData = pickle.dumps(statusReport)
        returnMsg = communication_pb2.genericPickleMsg(data=binaryData)
        logging.info('Return message packaged')
        return returnMsg
        pass


# logging.basicConfig(level=logging.INFO)
# vidCapture = VideoCaptureAsync(src=0, useUSBCam=True)
# vidCapture.start()
# time.sleep(1)
# vidCapture.read()
# communicationServicer = CommunicationServicer()
# communicationServicer.setImageStreamingQ(vidCapture.imageQueueForStreaming)
# myServer = Server(communicationServicer)
# myServer.start()
# # serve()
# time.sleep(100000)
