#include "comPinMap.h"

UARTSerial* createSerial(UARTSerialPort port)
{
    
    if (port == UARTSerialPort::Serial4)
        return new UARTSerial(PB_9, PB_8, 115200); // serial port 4
    else
        return NULL;
    // to be implemented to make life easier
}