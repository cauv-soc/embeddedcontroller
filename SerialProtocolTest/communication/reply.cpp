#include "reply.h"

int replySizeMapper(Reply instruction)
{
    switch(instruction)
    {
        case Reply::END_MESSAGE:
            return 0;
        case Reply::ECHO:
            return 1;
        case Reply::IMU_READING:
            return 6;
        case Reply::RAW_SPEED:
            return 6;
    }
}
