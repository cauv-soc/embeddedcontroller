#ifndef COMPONENTTEST_H
#define COMPONENTTEST_H

#include "mbed.h"
#include "myDebug.h"

// these tests are meant to be non-blocking
// they run once and then returns
void singleESCTest(PinName pwmPin);

void thrustersTest();

void imuTest();


#endif