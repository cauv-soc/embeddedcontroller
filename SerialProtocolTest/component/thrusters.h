#ifndef THRUSTER_H
#define THRUSTER_H

#include "mbed.h"
#define N_THRUSTER 2

class MultipleESC
{
    MultipleESC(PinName* pwmPins);
    void setSpeed(int* pwmPins);
};

#endif