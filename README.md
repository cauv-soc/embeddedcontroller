# embeddedController

## Introduction
This repo houses both the python (pc end) and the C++ (mbed end) code.
They are placed in their respective folders.
## To-to
*   <del>Buy a FTDI. The mbed code will be adjusted 
    such that the actual communication happens via a normal serial port with a FTDI, 
    while the debug message is printed out via the USB port. </del>(DONE!)
*   <del>Test myDebug, and use it to simplify non-blocking debug printing. </del> (Done!)
*   <del>Refactor test the instructionSet.h and use the auto-generated
    c++ code (in the utility folder) </del> (Done!)
*   Figure out how to make .gitignore work properly
*   Clean up repo
*   Test that the current "test framework" works (kind of done...)
*   Refine the test frame work. Name everything nicely, 
    and match the name on the python side.
    Possibly version the tests as well.
*   Complete comPinMap and update the rest of the code for simplification
*   Do the sending code on the pc side properly 
*   <del>Do the sending code on the mbed side (needs testing)</del>
*   <del>Do the receiving code in python on the pc side (threaded) (so much pain :( ) </del>
*   Basic tests with the communication library are done.
    Probably need to come up with some more vigorous tests.
*   There may be point in improving the logging thing a bit.
    Basically get nicer format with color and so on. (Low priority)
*   Starting looking at the response of the mbed on receiving some information
*   Maybe there is some point in wrapping mbed serial library, 
    but this has a very low priority.
*   Now try to do something with gRPC!
## Some cross-check that needed to be done
*   Check comPinMap to make sure that everything is keyed in correctly

## Have a look at Wiki as well!