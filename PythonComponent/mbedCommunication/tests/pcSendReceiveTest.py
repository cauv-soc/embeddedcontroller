from PythonComponent.mbedCommunication.serialHandler import *
import time
import logging

# takes about 10 ms for the echo to finish
# remember to take out the debug delay time on mbed
# otherwise, a ~5 s delay will be seen because of that
logging.basicConfig(level=logging.DEBUG)
serialHandler = SerialHandler()
serialHandler.sendMsg(Instruction.ECHO, [1])
time.sleep(1)
serialHandler.sendMsg(Instruction.ECHO, [2])
while True:
    time.sleep(1)
