import cv2
import numpy as np
import time
import math
import serial
import RPi.GPIO as GPIO
import time
from picamera.array import PiRGBArray
from picamera import PiCamera
from EstablishSerial import serialLink

def transformCoordinates(x, y, centre):
    newx = x - centre[0]
    newy = centre[1] - y
    r = (newx ** 2 + newy ** 2) ** 0.5
    if newx == 0:
        newx = 1

    theta = math.atan2(newy, newx) * 360 / (2 * 3.14)
    theta = (theta - 45) % 360
    if theta > 180:
        theta = theta - 360

    return (theta, r)


def interpolate(r):
    readings = [10, 15, 20, 25, 30, 35, 40, 45]
    calibration = [20, 80, 103, 123, 140, 150, 161, 167]
    if r > calibration[-1]:
        adjusted = readings[-1] + 1
        return adjusted
    if r < calibration[0]:
        adjusted = 10
        return adjusted
    for i in range(len(calibration) - 1):
        if r > calibration[i]:
            adjusted = (r - calibration[i]) / (calibration[i + 1] - calibration[i]) * 5 + (10 + 5 * i)

    return adjusted


# upperOrange = np.array([10,255,255])
# lowerOrange = np.array([0,150,150])
upperOrange = np.array([20, 255, 255])
lowerOrange = np.array([0, 50, 50])
upperField = np.array([140, 255, 255])
lowerField = np.array([50, 30, 30])
upperBlack = np.array([255, 255, 30])
lowerBlack = np.array([0, 0, 0])

cnts = []
centerpoint = [240, 240]
while (1):
    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        serialComm.add(255)
        serialComm.add(255)
        serialComm.add(255)

        # grab the raw NumPy array representing the image, then initialize the timestamp
        # and occupied/unoccupied text
        img = frame.array
        rawCapture.truncate(0)
        image = img[0:480, 80:560]
        k = cv2.waitKey(30) & 0xff
        if k == 27:
            break
        start = time.time()
        hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        end1 = time.time()
        # print(end1-start," hsv")
        mask = cv2.inRange(hsv, lowerOrange, upperOrange)
        playingField = cv2.inRange(hsv, lowerField, upperField)
        dilateKernel = np.ones((7, 7), np.uint8)
        playingField = cv2.morphologyEx(playingField, cv2.MORPH_OPEN, dilateKernel, iterations=2)
        erodeKernel = np.ones((7, 7), np.uint8)
        playingField = cv2.erode(playingField, erodeKernel, iterations=1)
        _, playingcnts, _ = cv2.findContours(playingField, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        _, cnts, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        blackBoundary = cv2.inRange(hsv, lowerBlack, upperBlack)
        blackBoundary = cv2.morphologyEx(blackBoundary, cv2.MORPH_OPEN, dilateKernel, iterations=2)

        _, boundary, _ = cv2.findContours(blackBoundary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        # cv2.drawContours(image, boundary, -1, (0,0,255),3)
        # cv2.drawContours(image, playingcnts, -1, (0,0,255),3)
        end2 = time.time()
        end = 0;
        # print(end2-end1," contour")
        if len(cnts) > 0:
            # find the largest contour in the mask, then use
            # it to compute the minimum enclosing circle and
            # centroid
            c = max(cnts, key=cv2.contourArea)
            ((x, y), radius) = cv2.minEnclosingCircle(c)
            M = cv2.moments(c)
            if M["m00"] != 0:
                center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
                end3 = time.time()
                # print(end3-end2, "centre")

                # only proceed if the radius meets a minimum size
                if radius > 3:
                    # draw the circle and centroid on the frame,
                    # then update the list of tracked points
                    cv2.circle(image, (int(x), int(y)), int(radius),
                               (0, 255, 255), 2)
                    cv2.circle(image, center, 5, (0, 0, 255), -1)
                    polar = transformCoordinates(int(x), int(y), centerpoint)
                    # polar[1] = interpolate(polar[1])
                    polar = (int(polar[0])), int(polar[1])
                    end = time.time()
                    print(polar)
                    # print(end4-end3," polar")

                    serialComm.addDetectedObject(5, polar[0], polar[1])
                    serialComm.add(254)
                    serialComm.add(254)
                    serialComm.add(254)
                    serialComm.transmit()

        # print(end1-start,"hsv")
        # print(end2-end1,"contour")
        # print(end3-end2,"center")
        # print(end-end3,"polar")
        # print(end-start)
        cv2.imshow('frame', image)

cap.release()
cv2.destroyAllWindows()
cv2.waitKey(1)
