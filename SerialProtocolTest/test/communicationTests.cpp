#include "communicationTests.h"
#include "serialHandler.h"

void myDebugTest()
{
    Timer timer;
    timer.start(); // don't forget to start the timer
    while (true)
    {
        // test that the sending works properly
        myDebug("HelloWorld\r\n");
        thread_sleep_for(500);

        // test that the formatting can work properly
        myDebug("This is a number %d \r\n", 123);
        thread_sleep_for(500);

        // show that it is indeed non-blocking
        timer.reset();
        myDebug("Hello World.\r\n");
        thread_sleep_for(1);
        float timeElapsed = timer.read();
        myDebug("This takes %f ms.\r\n", timeElapsed*1000);
        thread_sleep_for(500);

        timer.reset();
        myDebug("This is a very very very very very long Hello World message.\r\n"); // roughly 60 characters, ~1ms
        timeElapsed = timer.read();
        myDebug("This takes %f ms.\r\n", timeElapsed*1000);
        thread_sleep_for(500);
    }
}

void mbedReceiveTest()
{
    UARTSerial pcCom(PB_9, PB_8, 115200); // serial port 4
    pcCom.set_baud(115200);
    pcCom.set_blocking(false);
    SerialHandler serialHandler(&pcCom);//, &pcDebugPort);
    InstructionContainer newInstruction;

    // just to let it know that it is alive
    for(int i = 0; i < 10; i++) 
    {
        myDebug("This is the debug port.\r\n");
        char buf[100];
        sprintf(buf,"This is the communication port.\r\n");
        pcCom.write(buf, strlen(buf));
        // thread_sleep_for(1000);
    }

    // scan port and information will be printed out via debug port
    // as of now, assume that the debug flag is turned on
    // may implement a switchable debud flag later
    while (true)
        serialHandler.scanPort(&newInstruction);
}

void mbedReceiveEchoTest()
{
    UARTSerial pcCom(PB_9, PB_8, 115200); // serial port 4
    pcCom.set_baud(115200);
    pcCom.set_blocking(false);
    SerialHandler serialHandler(&pcCom);//, &pcDebugPort);
    InstructionContainer newInstruction;

    // just to let it know that it is alive
    for(int i = 0; i < 10; i++) 
    {
        myDebug("This is the debug port.\r\n");
        char buf[100];
        sprintf(buf,"This is the communication port.\r\n");
        pcCom.write(buf, strlen(buf));
        // thread_sleep_for(1000);
    }

    // scan port and information will be printed out via debug port
    // as of now, assume that the debug flag is turned on
    // may implement a switchable debud flag later
    while (true)
    {
        // read sensors
        // check for incomming msg
        while(serialHandler.scanPort(&newInstruction) == PortScanResult::FRAME_READY)
        {
            // handle it accordingly
            if (newInstruction.instruction == Instruction::ECHO)
                serialHandler.reply(Reply::ECHO, newInstruction.data_field);
        }
    }
}

void pcReceiveTest()
{
    UARTSerial pcCom(PB_9, PB_8, 115200); // serial port 4
    pcCom.set_baud(115200);
    pcCom.set_blocking(false);
    SerialHandler serialHandler(&pcCom);//, &pcDebugPort);
    InstructionContainer newInstruction;

    // just to let it know that it is alive
    for(int i = 0; i < 10; i++) 
    {
        myDebug("This is the debug port.\r\n");
        char buf[100];
    }

    while (true)
    {
        char val[1];
        for (int i = 0; i < 10; i++)
        {
            val[0] = i;
            serialHandler.reply(Reply::ECHO, val);
            thread_sleep_for(1000);
        }
        thread_sleep_for(10000);
        
    }
}

void pcSendReceiveTest()
{
    mbedReceiveEchoTest();
}