import math

class position:
    def __init__(self, _x, _y):
        self.x = _x
        self.y = _y

class maker:
    #should be given in the anti-clockwise direction
    def __init__(self, _pos1, _pos2, _midPos, _color):
        self.pos1 = _pos1
        self.pos2 = _pos2
        self.midPos = _midPos
        self.color = _color
    # color ind
    # red = 0
    # blue = 1
    # purple = 2
    # yellow = 3
    # green = 4

    def calAngle(self,robotPose):
        angle1 = math.round(math.atan2(self.pos1.y - robotPose.y, self.pos1.x - robotPose.x) / 3.1415926 * 180)
        angle2 = math.round(math.atan2(self.pos2.y - robotPose.y, self.pos2.x - robotPose.x) / 3.1415926 * 180)
        angleRange = (angle2 + 360 - angle1) % 360
        midAngle = (angle1 + angleRange / 2) % 360
        return midAngle,angleRange

    def calDist(self,robotPose):
        distMin = math.sqrt((self.midPos.x - robotPose.x)*(self.midPos.x - robotPose.x) + (self.midPos.y - robotPose.y)*(self.midPos.y - robotPose.y))
        distMax = distMin * (1.0 + 14.0/22.0)
        midDist = (distMin + distMax) / 2.0
        distRange = distMax - distMin
        return midDist,distRange

class observedMarker:
    def __init__(self, _distance, _angle, _color):
        self.distance = _distance
        self.angle = _angle
        self.color = _color

class triangulator:
    red = 0
    blue = 1
    purple = 2
    yellow = 3
    green = 4

    grid_size_x =

    def __init__(self):
        self.markerList = []
        # red markers
        self.markerList.append(maker(position(-90, 21),position(-90, 0),position(-90, 10),self.red))
        self.markerList.append(maker(position(-90, -99), position(-69, -120), position(-90, -120), self.red))
        # other colors ... (need to type in one by one)

    def calGaussian(self,deltaX,stdDeviation):
        return 1.0/ math.sqrt(2.0 * 3.1415926) / stdDeviation * math.exp(deltaX / stdDeviation)

    def calLikelihood(self, obsMarker, robotPose):
        maxLikelihood = 0
        for knownMarker in self.markerList:
            #select the marker with the correct color
            if (knownMarker.color != obsMarker.color):
                continue
            midAngle, angleRange = knownMarker.calAngle(robotPose)
            midDist, distRange = knownMarker.calAngle(robotPose)
            angleDifference = math.min((obsMarker.anlge - midAngle + 360) % 360, (-obsMarker.anlge + midAngle + 360)%360)
            prob = self.calGaussian(angleDifference, angleRange / 2.0)
            # merging distance data currently not implemented
            # prob = self.calGaussian(obsMarker.distance - midDist, distRange) * prob
            if (prob > maxLikelihood):
                maxLikelihood = prob
        return maxLikelihood

    def findMostLiklyPos(self, obsMarkerList):
        for obsMarker in obsMarkerList:
        return








