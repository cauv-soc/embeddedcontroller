class Reply:
    END_MESSAGE = 0
    ECHO = 1
    IMU_READING = 2
    RAW_SPEED = 3

    def __init__(self):
        pass

    @staticmethod
    def SizeMapper(instruction):
        if instruction == Reply.END_MESSAGE:
            return 0
        if instruction == Reply.ECHO:
            return 1
        if instruction == Reply.IMU_READING:
            return 6
        if instruction == Reply.RAW_SPEED:
            return 6

