#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#define MAX_DATA_FIELD_LENGTH 6

enum class Instruction
{
END_MESSAGE,
ECHO,
SET_SPEED_RAW,
SET_IMU_REPORT,
CONTROLLER_INPUT
};

int instructionSizeMapper(Instruction instruction);

#endif