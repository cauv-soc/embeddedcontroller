#ifndef REPLY_H
#define REPLY_H

#define MAX_DATA_FIELD_LENGTH 6

enum class Reply
{
END_MESSAGE,
ECHO,
IMU_READING,
RAW_SPEED
};

int replySizeMapper(Reply instruction);

#endif