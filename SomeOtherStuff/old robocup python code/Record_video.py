import numpy as np
import cv2
# this will allow the recording of video from camera
# the file will be saved in avi which can be read back in again using opencv
# the codec (in the case, MJPG) is platform dependent. The same code may not be runnable on other platforms
# https://stackoverflow.com/questions/29317262/opencv-video-saving-in-python


def recordVideo(cameraInd, outputFileName):
    cap = cv2.VideoCapture(cameraInd)
    # Define the codec and create VideoWriter object
    fourcc = cv2.VideoWriter_fourcc(*'MJPG')
    out = cv2.VideoWriter(outputFileName,fourcc, 20.0, (640,480))
    while cap.isOpened():
        ret, frame = cap.read()
        if ret:
            out.write(frame)
            cv2.imshow('frame',frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        else:
            break
    # Release everything if job is finished
    cap.release()
    out.release()
    cv2.destroyAllWindows()