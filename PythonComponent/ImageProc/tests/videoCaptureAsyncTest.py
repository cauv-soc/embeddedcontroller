from PythonComponent.ImageProc.videoCaptureAsync import VideoCaptureAsync
import cv2
import time
import logging

logging.basicConfig(level=logging.DEBUG)
vidCapture = VideoCaptureAsync(src=0, useUSBCam=True)
vidCapture.start()
time.sleep(1)
t0 = time.time()
tMax = 10  # let the program run for 60s
cnt = 0

while time.time() - t0 < tMax:
    newFrame = vidCapture.read()
    cnt = cnt + 1
    cv2.imwrite('test' + str(cnt) + '.jpg', newFrame)

vidCapture.stop()
print('fps:' + str(cnt/tMax))

