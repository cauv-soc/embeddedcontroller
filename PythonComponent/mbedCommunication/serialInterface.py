import serial.tools.list_ports
import serial
import sys
import time
import struct

# issue: how to narrow down the scope for the except so that it does not catch other error
# may try: except serial.SerialException:


class SerialInterface(object):
    def __init__(self, identifier='USB'):
        self.identifier = identifier
        self.mbedAvailable = False
        self.ser = None
        self.buffer = bytes([])

        self.initSerial()
        if not self.mbedAvailable:
            self.handleSerialException()
        # while not self.mbedAvailable:
        #     time.sleep(500)
        #     self.initSerial()

    def initSerial(self):
        self.ser = None
        self.mbedAvailable = False  # actually this function is not supposed to be called with mbed available
        serialPortMbed = None

        ports = list(serial.tools.list_ports.comports())
        for p in ports:
            print(p)
            if self.identifier in p[1]:  # this is the identifier used to locate mbed
                serialPortMbed = p[0]
                print('mbed found, identifier used ', end='')
                print(self.identifier)
                self.mbedAvailable = True

        # initialize the serial port when a Mbed is found among USB devices
        if self.mbedAvailable:
            self.ser = serial.Serial(
                port=serialPortMbed,
                baudrate=115200,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                timeout=0.1
            )
            print('port open')
        else:
            print('Mbed Not Found')

    def handleSerialException(self):
        print('Mbed connection lost')
        self.mbedAvailable = False
        counter = 0
        while not self.mbedAvailable:
            time.sleep(1)
            counter = counter + 1
            print('Trying to re-establish connection. Try %d', counter, end='     ')
            self.initSerial()
            if self.mbedAvailable:
                print('connection re-established')
                break
            else:
                print('failed')
                if counter > 20:
                    print('We give up')
                    sys.exit()

    def add(self, number):
        self.buffer = self.buffer + struct.pack(b'B', number)

    def transmit(self):
        try:
            self.ser.write(self.buffer)
            self.buffer = self.buffer = bytes([])
        except serial.SerialException:
            print("error in transmit")
            self.handleSerialException()

    def available(self):
        try:
            if self.ser.inWaiting() > 0:
                return True
            else:
                return False
        except serial.SerialException:
            print("error in reading")
            self.handleSerialException()
            return False

    # this is a blocking read
    def read(self):
        try:
            while self.ser.inWaiting <= 0:
                pass
            self.ser.read()
        except serial.SerialException:
            print("error in reading")
            self.handleSerialException()
            return self.read()  # not exactly sure if this is a good practice at all...

    # debug functions
    def debugReadNumber(self):
        try:
            while self.ser.inWaiting() > 0:
                print(ord(self.ser.read()))
        except serial.SerialException:
            print("error in reading")
            self.handleSerialException()

    def debugReadStr(self):
        try:
            while self.ser.inWaiting() > 0:
                c = self.ser.read()
                try:
                    print(c.decode("utf-8"), end='')
                except UnicodeDecodeError:
                    print("\nnot string")
        except serial.SerialException:
            print("error in reading")
            self.handleSerialException()
