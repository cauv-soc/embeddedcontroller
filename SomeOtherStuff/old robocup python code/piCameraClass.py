import cv2
import numpy as np
import time
import math
import serial
import RPi.GPIO as GPIO
import time
from picamera.array import PiRGBArray
from picamera import PiCamera
from EstablishSerial import serialLink


class piCameraClass(object):
    def __init__(self):
        self.size = (640,480)
        self.camera = PiCamera()
        self.camera.resolution = self.size
        self.camera.framerate = 32
        self.rawCapture = PiRGBArray(self.camera, size=self.size)
        time.sleep(1)

    def getImage(self):
        with self.camera.PiCamera() as camera:
            with self.ccamera.array.PiRGBArray(self.camera) as stream:
                while True:
                    self.camera.capture(stream, format='bgr', use_video_port=True)
                    image = stream.array
                    cv2.imshow('frame', image)
                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        break
                    # reset the stream before the next capture
                    stream.seek(0)
                    stream.truncate()
                    time.sleep(1)


    def test(self):
        while(1):
            img = self.getImage()
            cv2.imshow('win',img)
            time.sleep(1)