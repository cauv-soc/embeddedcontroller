# Content
## old robocup python code
The folder includes the python code I wrote for robocup 2018.
The code contains:
-   Serial handling on the python side
-   Serial recovery from accidental loss of communication
-   Some color based identification for computer vision

It is for reference only, and you cannot run it on a pc.
It is meant to run on a Raspi with a Raspi camera and a Teensy3.5 attached.