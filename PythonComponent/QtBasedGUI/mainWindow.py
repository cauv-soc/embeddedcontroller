from PyQt5.QtWidgets import QMainWindow, QApplication, QLabel, QVBoxLayout
from PyQt5.QtWidgets import QWidget
from PythonComponent.QtBasedGUI.utilities.ImgConversion import convertImg

class Window(QMainWindow):
    def __init__(self):
        super(Window, self).__init__()  # No idea why need to set the flag. Can be safely ignored
        self.setWindowTitle("Some title")
        self.tempLabel = QLabel()
        QtImg = convertImg()
        self.tempLabel.setPixmap(QtImg)
        # self.tempLabel.setText("HelloWorld")
        # the central widget
        centralArea = QWidget()
        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.tempLabel)
        # mainLayout.addWidget(GLWindow()) # multiple GL objects ok. Just don't do any strange setting
        centralArea.setLayout(mainLayout)
        self.setCentralWidget(centralArea)
        self.show()


app = QApplication([])
window = Window()
app.exec_()
