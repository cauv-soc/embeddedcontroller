#include "myDebug.h"

// https://stackoverflow.com/questions/1433204/how-do-i-use-extern-to-share-variables-between-source-files
// some note about not declaring any variable in a header files
// otherwise, use extern keyword
UARTSerial* debugCom = NULL;//(PB_9, PB_8, 115200); // serial port 4
bool debugComInit = false;

void myDebug ( const char * format, ... )
{
    if (!debugComInit)
    {
        debugCom = new UARTSerial(DEBUG_COM_TX, DEBUG_COM_RX, 115200); // serial port 4
        debugCom->set_baud(115200);
        debugCom->set_blocking(false);
        debugComInit = true;
        //myDebug("HelloWorld");
    }
    char buffer[256];
    va_list args;
    va_start (args, format);
    vsprintf (buffer,format, args);
    debugCom->write(buffer, strlen(buffer));
    va_end (args);
}