# the segment to track the ball
import cv2
import numpy as np
import time
import math
import serial
import RPi.GPIO as GPIO
import time
from picamera.array import PiRGBArray
from picamera import PiCamera

camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(640, 480))
time.sleep(0.1)


def transformCoordinates(x,y,centre):
    newx = x - centre[0]
    newy = centre[1] - y
    r = (newx**2 + newy**2)**0.5
    if newx == 0:
        newx = 1

    theta = math.atan2(newy,newx)*360/(2*3.1415926)
    theta = (theta - 45)%360
    if theta > 180:
        theta = theta - 360

    return (theta, r)




def printOut(rList, sizeList):
    file = open("output.txt","w")
    file.write("{")
    for i in range(1,len(rList)):
        file.write("{%5d,%5d},"% (rList[i],sizeList[i])),
    file.write("{0,0}}")
    file.close()

#fitted contstants from the data
A = 4002.793501060018
B = 0.010264345431405126
C = 79.96217130602263
maxTolerance = 1.4
sizeStd = 0.4

def sizeProb(distance, area):
    expectedArea = 0.5*A*math.exp(-B*(distance - C))
    if (area < 1.4 * expectedArea):
        return 1.0
    if (area > expectedArea):
        error = (area - expectedArea) / area
        return math.exp(-(error - maxTolerance + 1)**2/sizeStd)

minTolerance = 0.8
shapeStd = 0.2
def shapeProb(area,perimeter):
    circularity = area / (perimeter / 2.0 / 3.14315926)**2
    if (circularity > minTolerance):
        return 1
    if (circularity < minTolerance):
        return math.exp(-(circularity - minTolerance) ** 2 / shapeStd)

def ballProb(area, perimeter, distance):
    return shapeProb(area*perimeter) * sizeProb(distance,area)

centerpoint = [240,240]
def transformCoordinates2(x,y,centre):
    newx = x - centre[0]
    newy = centre[1] - y
    r = (newx**2 + newy**2)**0.5
    if newx == 0:
        newx = 1

    theta = math.atan2(newy,newx)*360/(2*3.14)
    theta = (theta - 45)%360
    if theta > 180:
        theta = theta - 360

    return (theta, r)

def testingFunctions(contours):
    maxCnt =[]
    maxProb = 0
    for contour in contours:
        M = cv2.moments(contour)
        center = [1,1]
        if M["m00"] != 0:
            center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
        theta,r = transformCoordinates2(center[0],center[1],centerpoint)
        area = cv2.contourArea(contour)
        perimeter = cv2.arcLength(contour,True)
        print(center),
        print(" "),
        print((theta,r)),
        print(" sizeProb"),
        print(sizeProb(r,area)),
        print(" shapeProb"),
        print(shapeProb(area,perimeter))

upperOrange = np.array([20,255,255])
lowerOrange = np.array([0,80,80])
upperOrange2 = np.array([160,255,255])
lowerOrange2 = np.array([179,80,80])


cnts = []
centerpoint = [240,240]
rList = []
sizeList = []
counter = 0
while(1):
    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        #rawCapture.truncate(0)
        img = frame.array
        #cv2.imshow("win",img)
        #time.sleep(1)
        rawCapture.truncate(0)
        image = img[0:480, 80:560]
        k = cv2.waitKey(30) & 0xff
        if k == 27:
            break
        hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        mask1 = cv2.inRange(hsv, lowerOrange, upperOrange)
        mask2 = cv2.inRange(hsv, lowerOrange2, upperOrange2)
        mask = cv2.addWeighted(mask1,1,mask2,1,0)
        dilateKernel = np.ones((3,3),np.uint8)
        mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN,dilateKernel, iterations=1)
        erodeKernel = np.ones((3,3),np.uint8)
        mask = cv2.erode(mask, erodeKernel, iterations=1)
        cv2.imshow("frame", mask)
        #time.sleep(0.2);
        _, cnts,    _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        if len(cnts) > 0:
            # find the largest contour in the mask, then use
            # it to compute the minimum enclosing circle and
            # centroid
            c = max(cnts, key=cv2.contourArea)
            M = cv2.moments(c)
            center = (0,0)
            if M["m00"] != 0:
                center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
            _, r =transformCoordinates(center[0],center[1],centerpoint)
            rList.append(r)
            sizeList.append(cv2.contourArea(c))
            counter = counter + 1
            #print(counter)
        #print(counter)
        if (counter > 100):
            printOut(rList,sizeList)
            exit(0)

printOut(rList,sizeList)
