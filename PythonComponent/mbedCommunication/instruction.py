class Instruction:
    END_MESSAGE = 0
    ECHO = 1
    SET_SPEED_RAW = 2
    SET_IMU_REPORT = 3
    SET_SPEED_REPORT = 4
    CONTROLLER_INPUT = 5

    def __init__(self):
        pass

    @staticmethod
    def SizeMapper(instruction):
        if instruction == Instruction.END_MESSAGE:
            return 0
        if instruction == Instruction.ECHO:
            return 1
        if instruction == Instruction.SET_SPEED_RAW:
            return 6
        if instruction == Instruction.SET_IMU_REPORT:
            return 1
        if instruction == Instruction.SET_SPEED_REPORT:
            return 1
        if instruction == Instruction.CONTROLLER_INPUT:
            return 5
