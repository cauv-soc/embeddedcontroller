# the segment to track the ball
import cv2
import numpy as np
import math


def transformCoordinates(x,y,centre):
    newx = x - centre[0]
    newy = centre[1] - y
    r = (newx**2 + newy**2)**0.5
    if newx == 0:
        newx = 1

    theta = math.atan2(newy,newx)*360/(2*3.1415926)
    theta = (theta - 45)%360
    if theta > 180:
        theta = theta - 360

    return (theta, r)




def printOut(rList, sizeList):
    file = open("output.txt","w")
    file.write("{")
    for i in range(1,len(rList)):
        file.write("{%5d,%5d},"% (rList[i],sizeList[i])),
    file.write("{0,0}}")
    file.close()

#fitted contstants from the data
A = 4002.793501060018
B = 0.010264345431405126
C = 79.96217130602263
maxTolerance = 1.4
sizeStd = 0.4

def sizeProb(distance, area):
    expectedArea = 0.5*A*math.exp(-B*(distance - C))
    if (area < 1.4 * expectedArea):
        return 1.0
    if (area > expectedArea):
        error = (area - expectedArea) / area
        return math.exp(-(error - maxTolerance + 1)**2/sizeStd)

minTolerance = 0.8
shapeStd = 0.2
def shapeProb(area,perimeter):
    if (perimeter == 0):
        return 0
    circularity = area / (perimeter / 2.0 / 3.14315926)**2 / 3.1415926
    print("circularity")
    print(circularity)
    if (circularity > minTolerance):
        return 1
    if (circularity < minTolerance):
        return math.exp(-(circularity - minTolerance) ** 2 / shapeStd)

def ballProb(area, perimeter, distance):
    return shapeProb(area*perimeter) * sizeProb(distance,area)

centerpoint = [240,240]
def transformCoordinates2(x,y,centre):
    newx = x - centre[0]
    newy = centre[1] - y
    r = (newx**2 + newy**2)**0.5
    if newx == 0:
        newx = 1

    theta = math.atan2(newy,newx)*360/(2*3.14)
    theta = (theta - 45)%360
    if theta > 180:
        theta = theta - 360

    return (theta, r)

def testingFunctions(contours, img):
    maxCnt =[]
    maxProb = 0
    for contour in contours:
        M = cv2.moments(contour)
        center = [1,1]
        if M["m00"] != 0:
            center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
        theta,r = transformCoordinates2(center[0],center[1],centerpoint)
        area = cv2.contourArea(contour)
        perimeter = cv2.arcLength(contour,True)
        print("New Contour")
        print("center"),
        print(center),
        print("polar")
        print((theta,r)),
        print("sizeProb"),
        print(sizeProb(r,area)),
        print("shapeProb"),
        print(shapeProb(area,perimeter))
        print("")
        cv2.imshow("with contour", img)
        cv2.waitKey(0)
        cv2.drawContours(img, [contour], 0, (0, 255, 0), 3)
        cv2.imshow("with contour", img)
        cv2.waitKey(0)

upperOrange = np.array([20,255,255])
lowerOrange = np.array([0,80,80])
upperOrange2 = np.array([160,255,255])
lowerOrange2 = np.array([179,80,80])


cnts = []
centerpoint = [240,240]
rList = []
sizeList = []
counter = 0
while(1):
    frame = cv2.imread("test.png",cv2.IMREAD_COLOR)
    img = cv2.resize(frame, (640, 480), interpolation=cv2.INTER_CUBIC)
    #img = frame
    #print(img.size())
    cv2.imshow("win",img)
    cv2.waitKey(0)

    image = img[0:480, 40:510]
    cv2.imshow("win",image)
    cv2.waitKey(0)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    mask1 = cv2.inRange(hsv, lowerOrange, upperOrange)
    mask2 = cv2.inRange(hsv, lowerOrange2, upperOrange2)
    mask = cv2.addWeighted(mask1,1,mask2,1,0)
    dilateKernel = np.ones((3,3),np.uint8)
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN,dilateKernel, iterations=1)
    erodeKernel = np.ones((3,3),np.uint8)
    mask = cv2.erode(mask, erodeKernel, iterations=1)
    cv2.imshow("mask", mask)
    cv2.waitKey(0)
    #time.sleep(0.2);
    _, cnts,    _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    if len(cnts) > 0:
        testingFunctions(cnts,image)
