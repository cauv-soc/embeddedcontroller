import math
import numpy as np
import cv2

centerpoint = [240,240]
def transformCoordinates(x, y, centre = centerpoint):
    newx = x - centre[0]
    newy = centre[1] - y
    r = (newx ** 2 + newy ** 2) ** 0.5
    if newx == 0:
        newx = 1

    theta = math.atan2(newy, newx) * 360 / (2 * 3.14)
    theta = (theta - 45) % 360
    if theta > 180:
        theta = theta - 360

    return (theta, r)


def interpolate(r):
    readings = [10, 15, 20, 25, 30, 35, 40, 45]
    calibration = [20, 80, 103, 123, 140, 150, 161, 167]
    if r > calibration[-1]:
        adjusted = readings[-1] + 1
        return adjusted
    if r < calibration[0]:
        adjusted = 10
        return adjusted
    for i in range(len(calibration) - 1):
        if r > calibration[i]:
            adjusted = (r - calibration[i]) / (calibration[i + 1] - calibration[i]) * 5 + (10 + 5 * i)

    return adjusted
