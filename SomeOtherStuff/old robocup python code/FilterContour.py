# the segment to track the ball
import cv2
import numpy as np
import math
from utilities import *
from imageProc import *
import time

FC_debug = True

#fitted contstants from the data
A = 4002.793501060018
B = 0.010264345431405126
C = 79.96217130602263
maxTolerance = 1.4
sizeStd = 0.4

def sizeProb(distance, area):
    expectedArea = 0.5*A*math.exp(-B*(distance - C))
    if (area < 1.4 * expectedArea):
        return 1.0
    if (area > expectedArea):
        error = (area - expectedArea) / area
        return math.exp(-(error - maxTolerance + 1)**2/sizeStd)

minTolerance = 0.8
shapeStd = 0.2
def shapeProb(area,perimeter):
    if (perimeter == 0):
        return 0
    circularity = area / (perimeter / 2.0 / 3.14315926)**2 / 3.1415926
    if (FC_debug):
        print("circularity")
        print(circularity)
    if (circularity > minTolerance):
        return 1
    if (circularity < minTolerance):
        return math.exp(-(circularity - minTolerance) ** 2 / shapeStd)

def ballProb(area, perimeter, distance):
    return shapeProb(area*perimeter) * sizeProb(distance,area)

def testingFunctions(contours, img):
    maxCnt =[]
    maxProb = 0
    for contour in contours:
        M = cv2.moments(contour)
        center = [1,1]
        if M["m00"] != 0:
            center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
        theta,r = transformCoordinates(center[0],center[1])
        area = cv2.contourArea(contour)
        perimeter = cv2.arcLength(contour,True)
        if (FC_debug):
            print("New Contour")
            print("center"),
            print(center),
            print("polar")
            print((theta,r)),
            print("sizeProb"),
            print(sizeProb(r,area)),
            print("shapeProb"),
            print(shapeProb(area,perimeter))
            print("")
            cv2.imshow("with contour", img)
            cv2.waitKey(0)
            cv2.drawContours(img, [contour], 0, (0, 255, 0), 3)
            cv2.imshow("with contour", img)
            cv2.waitKey(0)

# assume that there is something inside the cnts
def selectBall(img,cnts):
    #((x, y), radius) = cv2.minEnclosingCircle(c)
    maxProb = -1.0
    ballContour = cnts[0]
    for contour in cnts:
        # computing the properties of the contour
        M = cv2.moments(contour)
        if M["m00"] != 0:
            center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
        else:
            continue
        area = cv2.contourArea(contour)
        # demand a min area for it to be considered a ball
        if (area < 30):
            continue
        perimeter = cv2.arcLength(contour,True)
        theta, r = transformCoordinates(center[0], center[1])

        # select the contour with the maximum probability
        totalProb = sizeProb(r,area) * shapeProb(area,perimeter)
        if (totalProb > maxProb):
            maxProb = totalProb
            ballContour = contour

        # debug segment
        if (FC_debug):
            print("New Contour")
            print("center"),
            print(center),
            print("polar")
            print((theta,r)),
            print("sizeProb"),
            print(sizeProb(r,area)),
            print("shapeProb"),
            print(shapeProb(area,perimeter))
            print("total prob")
            print(totalProb)

    # indicate that no ball is found
    if (maxProb == - 1.0):
        return (0,-1.0)
    else:
        if (FC_debug):
            cv2.drawContours(img, [ballContour], 0, (0, 255, 0), 3)
            cv2.imshow("with contour", img)
            time.sleep(1)

        M = cv2.moments(ballContour)
        center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
        return transformCoordinates(center[0], center[1])

