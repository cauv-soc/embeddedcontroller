

# if the names are very long, it will take up more space...
# but most of the space will be taken up by the image anyway, do don't bother...
# note that a raw img frame 640*480 ~ 92 k before encoding
# not sure if it is worth the effort to encode the image because that takes time
class StatusReportContainer(object):
    def __init__(self):
        self.imgFrameRaw = None
        self.thrusterSpeed = None


class GSInstructionContainer(object):
    def __init__(self):
        pass