#include "instruction.h"

int instructionSizeMapper(Instruction instruction)
{
    switch(instruction)
    {
        case Instruction::END_MESSAGE:
            return 0;
        case Instruction::ECHO:
            return 1;
        case Instruction::SET_SPEED_RAW:
            return 6;
        case Instruction::SET_IMU_REPORT:
            return 1;
        case Instruction::CONTROLLER_INPUT:
            return 5;
    }
}
