from PyQt5.QtGui import QImage, QPixmap
import cv2


def convertImg(cvImg=None):
    if cvImg is None:
        cap = cv2.VideoCapture(0)
        ret, cvImg = cap.read()
        cap.release()
    height, width, channel = cvImg.shape
    bytesPerLine = 3 * width
    QtImg = QImage(cvImg.data, width, height, bytesPerLine, QImage.Format_RGB888).rgbSwapped()
    QtPixMap = QPixmap(QtImg)
    return QtPixMap
