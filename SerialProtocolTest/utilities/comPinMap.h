#ifndef COMPINMAP_H
#define COMPINMAP_H

#include "mbed.h"

enum class UARTSerialPort
{
    USBSerial,
    Serial1,
    Serial2,
    Serial3,
    Serial4
};


UARTSerial* createSerial(UARTSerialPort port);

#endif