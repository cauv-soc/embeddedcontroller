import socket


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        print('socket connection done')
        IP = s.getsockname()[0]
        print('socket name retrieved')
    except:
        IP = '127.0.0.1'
        print('Exception handled')
    finally:
        s.close()
    return IP


# print(get_ip())
