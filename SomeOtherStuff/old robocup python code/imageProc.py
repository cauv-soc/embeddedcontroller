import math
import numpy as np
import cv2
from  utilities import *
import time

imageProc_dubug = True


upperOrange = np.array([20,255,255])
lowerOrange = np.array([0,40,40])
upperOrange2 = np.array([179,255,255])
lowerOrange2 = np.array([160,40,40])
dilateKernel = np.ones((3, 3), np.uint8)
erodeKernel = np.ones((3, 3), np.uint8)

# ensure that both red and orange are detected. Further filtering algorithm will be used to extract out the ball
def thresholdingForOrange(rawImg):
    hsv = cv2.cvtColor(rawImg, cv2.COLOR_BGR2HSV)
    mask1 = cv2.inRange(hsv, lowerOrange, upperOrange)
    mask2 = cv2.inRange(hsv, lowerOrange2, upperOrange2)
    mask = cv2.addWeighted(mask1, 1, mask2, 1, 0)
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, dilateKernel, iterations=1)
    mask = cv2.erode(mask, erodeKernel, iterations=1)
    if (imageProc_dubug):
        cv2.imshow("frame", mask)
        time.sleep(0.1)
    _, cnts, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    return mask,cnts

# reserved for the thresholding of other colored markers
def thresholding(rawImg):
    pass


