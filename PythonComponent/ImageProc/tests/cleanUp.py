import os

# this basically removes all .jpg file saved under the current directory
fileList = [f for f in os.listdir('.') if f.endswith(".jpg")]
print(fileList)
for file in fileList:
    os.remove(file)
